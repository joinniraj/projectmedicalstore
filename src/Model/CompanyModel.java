/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rebel
 */
public class CompanyModel {
    private int cid;
    
     private String cname, ccountry, cemail,cphn,caddress;
     
     public CompanyModel(){
         
     }

    public CompanyModel(int cid, String cname, String ccountry, String cemail, String cphn, String caddress) {
        this.cid = cid;
        this.cname = cname;
        this.ccountry = ccountry;
        this.cemail = cemail;
        this.cphn = cphn;
        this.caddress = caddress;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCcountry() {
        return ccountry;
    }

    public void setCcountry(String ccountry) {
        this.ccountry = ccountry;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCphn() {
        return cphn;
    }

    public void setCphn(String cphn) {
        this.cphn = cphn;
    }

    public String getCaddress() {
        return caddress;
    }

    public void setCaddress(String caddress) {
        this.caddress = caddress;
    }
     
}
