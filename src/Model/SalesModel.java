/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rebel
 */
public class SalesModel {
    
    private int sid,quantity,price;
    
     private String mname, cname;
     
     public SalesModel(){     
     }

    public SalesModel(int sid, int quantity, int price, String mname, String cname) {
        this.sid = sid;
        this.quantity = quantity;
        this.price = price;
        this.mname = mname;
        this.cname = cname;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
     
    
    
    
    
    
}
