/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rebel
 */
public class MedicineModel {
    private int mid,quantity,price;
    
     private String mname, cname, ptype,exdate;
     
     public MedicineModel(){
         
     }

    public MedicineModel(int mid, int quantity, int price, String mname, String cname, String ptype, String exdate) {
        this.mid = mid;
        this.quantity = quantity;
        this.price = price;
        this.mname = mname;
        this.cname = cname;
        this.ptype = ptype;
        this.exdate = exdate;       
    }
    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getExdate() {
        return exdate;
    }

    public void setExdate(String exdate) {
        this.exdate = exdate;
    }

   
     
    
    
}
