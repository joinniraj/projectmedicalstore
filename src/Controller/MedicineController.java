/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DBConnection.DbConnect;
import Model.MedicineModel;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rebel
 */
public class MedicineController {

    public void addMedicine(MedicineModel ms) {
            boolean a=false;
        String sql = "INSERT INTO `mdcne`(`pname`, `cname`, `ptype`, `quantity`, `price`,`expirydate`)VALUES ('" + ms.getMname() + "','" + ms.getCname() + "','" + ms.getPtype() + "','" + ms.getQuantity() + "','" + ms.getPrice() + "','" + ms.getExdate() + "')";
        System.out.println(sql);
        try {
            Statement st = DbConnect.DbConnection();
            st.executeUpdate(sql);
           
        
            
            JOptionPane.showMessageDialog(null,"Inserted Sucessfully ");
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(this, "Insertion failed ");
            System.out.println("bhayena");
        }
       
    }

    public MedicineModel showValueDuringEdit(Object mid) {

        String sql = "SELECT * FROM `mdcne` WHERE mid=" + mid;
        System.out.println(mid);
        MedicineModel m = new MedicineModel();

        System.out.println("yahan aayo");
        try {

            Statement st = DbConnect.DbConnection();;
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                m.setMid(rs.getInt(1));
                m.setQuantity(rs.getInt(5));
                m.setPrice(rs.getInt(6));
                m.setMname(rs.getString(2));
                m.setCname(rs.getString(3));
                m.setPtype(rs.getString(4));
                m.setExdate(rs.getString(7));
            }

            // st.executeQuery(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return m;
    }

    public void editMedicine(MedicineModel mm) {

       // int mid = Integer.parseInt());
        String sql = "UPDATE `mdcne` SET `pname`='"+mm.getMname()+"',`cname`='"+mm.getCname()+"',`ptype`='"+mm.getPtype()+"',"
                + "`quantity`="+mm.getQuantity()+",`price`="+mm.getPrice()+","
                + "`expirydate`='"+mm.getExdate()+"' WHERE `mid`= "+mm.getMid();
        System.out.println(sql);
 

         try {
            Statement st = DbConnect.DbConnection();
            st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null,"Sucessfully Edited ");
            //System.out.println(stmt);
        } catch (Exception e) {
        }
    }

    public void deleteMedicine(Object a) {
        String sql = "DELETE FROM `mdcne` WHERE mid =" + a;
        System.out.println(sql);

        try {
            Statement st = DbConnect.DbConnection();
            st.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("final kaam vayena");
        }
    }

    public ArrayList<String> selectValueForCombo() {
        ArrayList<String> name = new ArrayList<String>();
        String sql1 = "SELECT * FROM `cmpny` WHERE 1";
        DbConnect db = new DbConnect();
        db.DbConnection();

        try {
            Statement st = DbConnect.DbConnection();
            ResultSet rs = st.executeQuery(sql1);
            //DefaultComboBoxModel dcbm = (DefaultComboBoxModel) jComboBox1.getModel();
            while (rs.next()) {
                name.add(rs.getString(2));

                //   dcbm.addElement(rs.getString("cname"));
            }

        } catch (Exception e) {

        }

        return name;
    }

    public void getValueFromCombo(Object a) {

        String query = "SELECT * FROM `cmpny` WHERE cname='" + a + "'";
        DbConnect db = new DbConnect();
        db.DbConnection();
        try {
            Statement st = DbConnect.DbConnection();
            //Statement st=con.createStatement();;
            ResultSet rs = st.executeQuery(query);
        } catch (Exception e) {
        }
    }

    public ArrayList<MedicineModel> displayMedicines() {
        ArrayList<MedicineModel> medicines = new ArrayList<MedicineModel>();
        String sql1 = "SELECT * FROM `mdcne` WHERE 1";
        DbConnect db = new DbConnect();

        try {
            Statement st = DbConnect.DbConnection();
            System.out.println(sql1);
            ResultSet rs = st.executeQuery(sql1);
            //DefaultComboBoxModel dcbm = (DefaultComboBoxModel) jComboBox1.getModel();
            while (rs.next()) {
                
                MedicineModel m = new MedicineModel();
                m.setMid(rs.getInt(1));
                m.setQuantity(rs.getInt(5));
                m.setPrice(rs.getInt(6));
                m.setMname(rs.getString(2));
                m.setCname(rs.getString(3));
                m.setPtype(rs.getString(4));
                m.setExdate(rs.getString(7));
                medicines.add(m);
                //   dcbm.addElement(rs.getString("cname"));

            }

        } catch (Exception e) {

        }
        return medicines;
    }

   
     public ArrayList<MedicineModel> searchMedicines(Object pname) {
        ArrayList<MedicineModel> al = new ArrayList<MedicineModel>();
   
         String sql = "SELECT * FROM `mdcne` WHERE `pname` LIKE '%" + pname + "%'";
         
        //MedicineModel m = new MedicineModel();

        DbConnect db = new DbConnect();
     
        try {
            Statement st = DbConnect.DbConnection();
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql);
           
            while (rs.next()) {               
                MedicineModel m = new MedicineModel();
                m.setMid(rs.getInt(1));
                m.setQuantity(rs.getInt(5));
                m.setPrice(rs.getInt(6));
                m.setMname(rs.getString(2));
                m.setCname(rs.getString(3));
                m.setPtype(rs.getString(4));
                m.setExdate(rs.getString(7));
                al.add(m);
                //   dcbm.addElement(rs.getString("cname"));

            }

        } catch (Exception e) {

        }
        return al;
    }

}
